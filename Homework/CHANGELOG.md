# Version History of `Homework`

## Version 2.4
 + reworked exercise system
 + new flag `forms`: point fields are forms

## Version 2.3
 + multilingual support (class option `lang=<language>`):
   + german / ngerman
   + english
 + reworked variable system
 + simplified base class

## Version 2.2
 + optional includes per sheet

## Version 2.1
 + new title layout
 + geometry reduces pagecount
 + modularize packages
 + simplify commands
 + add flag `fast` and support pdflatex

## Version 2.0
 + new directory structure
 + automatic sheet recognition
 + author recognition

## Version 1.2
 - unsupport non XeTeX / LuaTeX
 + improve rounding of points
 + introduce subfiles
 + clean up commands
 + clean up class

## Version 1.1
 + decimal points support
 + continous integration
 + more math commands
 + basic TikZ integration

## Version 1.0
 + basic template
 + integer points support
 + code listings
 + generic math commands
