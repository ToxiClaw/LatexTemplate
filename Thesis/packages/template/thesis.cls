% thesis.cls
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{thesis}

% Use KOMA article class
\RequirePackage{etoolbox, kvoptions}

\SetupKeyvalOptions {
	family = TX,
	prefix = tx,
}

\DeclareStringOption[german]{lang}[english]

\newtoggle{fast}
\DeclareOption{fast}{\toggletrue{fast}}

\newtoggle{forms}
\DeclareOption{forms}{\toggletrue{forms}}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrbook}}
\DeclareOption*{\ProcessKeyvalOptions{TX}}
\ProcessOptions\relax

\LoadClass{scrbook}

% Basic Includes
\RequirePackage{mathtools, amsmath, amssymb}
\RequirePackage[
	unicode,
	colorlinks=false,
	pdfborder={0 0 0},
	bookmarks,
	bookmarksopen,
	bookmarksnumbered,
	bookmarksdepth,
	%pdfmenubar=false,
	%pdftoolbar=false,
	pdfcreator={Adrian's LaTeX Template},
	draft=false,
]{hyperref}
\RequirePackage{subcaption}
\PassOptionsToPackage{final}{graphicx}

% Compiler specific configuration
\RequirePackage{ifxetex, ifluatex}
\newif\ifxetexorluatex
\ifxetex
	\xetexorluatextrue
\else \ifluatex
	\xetexorluatextrue
\else
	\xetexorluatexfalse
\fi \fi

\ifluatex
	% Specific Includes
	\RequirePackage[babelshorthands]{polyglossia}
	%\setdefaultlanguage{\txlang}
    \setdefaultlanguage{english}
	\setotherlanguage{english}
	\setotherlanguage{german}
	\selectlanguage{\txlang}
	\german@shorthands

	\RequirePackage[warnings-off={mathtools-colon,mathtools-overbracket}]{unicode-math}

	%% Fonts
	\nottoggle{fast}{
		\RequirePackage{fontspec}

		%\setmainfont[Ligatures=Common]{Charter}
		%\setmainfont[Ligatures=Common]{Lucida Bright OT}
		%\setmathfont[Ligatures=Common]{Lucida Bright Math OT}

		\setmainfont[Ligatures=Common]{Libertinus Serif}
		\setsansfont[Ligatures=Common]{Libertinus Sans}
		\setmathfont[Ligatures=Common]{Libertinus Math}

        %\setmainfont[Ligatures=Common]{Latin Modern Roman}
		%\setsansfont[Ligatures=Common]{Latin Modern Sans}
		%\setsansfont[Ligatures=Common]{Latin Modern Math}
		%\setmathfont[Ligatures=Common]{New Computer Modern Math}

        %\setmainfont[Ligatures=Common]{Stix Two Text}
        %\setmathfont[Ligatures=Common]{Stix Two Math}

		%\setmainfont[Ligatures=Common]{Schuss News Pro}

		%\setmathfont[range={\mathbb,\mathscr}]{XITS Math}
		%\setmathfont[range={\mathcal,\mathbfcal},StylisticSet=1]{XITS Math}

		\setmathfont[range={\mathfrak}]{XITS Math}
		\setmathfont[range={\mdwhtcircle,\mdwhtsquare,\mdwhtdiamond,\mdwhtlozenge,\smblkdiamond,\smwhtdiamond,\postalmark,\dicei,\diceii,\diceiii,\diceiv,\dicev,\dicevi,\sun,\twonotes,\lesseqgtr,\nlessgtr,\olessthan,\subsetcirc,\supsetcirc,\subsup,\lParen,\rParen,\lBrack,\rBrack,\intercal}]{XITS Math}
        \setmathfont[range={}]{Libertinus Math} % fix binomials

		%\newfontfamily\txtitlefont[Ligatures=Common]{Kerkis}
		\newfontfamily\txtitlefont[Ligatures=Common]{EB Garamond}
		%\newfontfamily\txtitlefont[Ligatures=Common]{Avenir}

		\RequirePackage[
			activate={true,nocompatibility},
			final,
			tracking=true,
			factor=1100,
			stretch=10,
			shrink=10,
		]{microtype}
	}{}

	\RequirePackage{csquotes}
	%% clashes with babelshorthands
	%\MakeOuterQuote{"} 
\else
	\toggletrue{fast}
	\ClassWarning{thesis}{Not supported compiler. Please use LuaLaTeX. This is for fast compilation only.}
%	\RequirePackage{uniinput}
	\RequirePackage[notext]{stix}
	\gdef\txtitlefont{}
	\gdef\txheadingfont{}
\fi

\clubpenalty10000
\widowpenalty10000
\interfootnotelinepenalty10000

% Other

\newcommand*{\tryinput}[1]{\IfFileExists{#1}{\input{#1}}{}}
\newcommand*{\tryusepackage}[1]{\IfFileExists{#1.sty}{\usepackage{#1}}{}}

% Local Includes
\RequirePackage{colors, variables, layout, theorems}

