# Version History of this project
Specific Version histories and informations can be found in the specific 'CHANGELOG' textfile within the subdirectory of interes.
Below is a list of the current Subdirectory-Version
* `Homework` has Version `2.4`
* `Booklet` has Version `1.2`
* `Presentation` has Version `1.2`
* `Paper` has Version `2.0`
* `Coverpages` has Version `1.0`
* `Thesis` has Version `1.0`

