# Version History of `Presentation`

## Version 1.2
 + complete overhaul

## Version 1.1
 + fix compatibility issues
 + update tikzstuff
 + better examples

## Version 1.0
 + basic template
 + presentation example
 + definitions and theorems
 + bibliography
