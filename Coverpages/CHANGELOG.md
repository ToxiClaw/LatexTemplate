# Version History of `Coverpages`

## Version 1.0
 + Add cover package with variables and page coordinates support
 + Add example.tex example and latexmkrc buildfile
 + Add basic styles a, b and c

