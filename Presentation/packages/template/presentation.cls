\NeedsTeXFormat{LaTeX2e}

% Use Beamer presentation class
\PassOptionsToClass{notheorems}{beamer}
\PassOptionsToClass {
	hyperref={
		unicode,
		colorlinks=false,
		pdfborder={0 0 0},
		bookmarks,
		bookmarksopen,
		bookmarksnumbered,
		bookmarksdepth
	},
    compress, beamer, aspectratio=169, english
}{beamer}

\LoadClass{beamer}
\ProvidesClass{presentation}

% Global Includes
\RequirePackage{mathtools, amsmath, amssymb}
\RequirePackage{graphicx, subcaption, adjustbox}
\RequirePackage{appendixnumberbeamer}
%\RequirePackage{booktabs, afterpage}

% Local Includes
\RequirePackage{colors, theorems}

% Class Macros
\RequirePackage{ifxetex, ifluatex, ifdraft}
\newif\ifxetexorluatex
\ifxetex
	\xetexorluatextrue
\else \ifluatex
	\xetexorluatextrue
\else
	\xetexorluatexfalse
\fi \fi

% Configuration
\ifxetexorluatex
	% Specific Includes
	\RequirePackage[babelshorthands]{polyglossia}
	\setdefaultlanguage{english}
	%\PolyglossiaSetup{english}{indentfirst=true}
    \SetLanguageKeys{english}{indentfirst=false}
	
	\RequirePackage[
		backend=biber,
		style=alphabetic,
		giveninits,
		url=true]{biblatex}
	
	\RequirePackage{csquotes}
	\RequirePackage{fontspec}
	\RequirePackage[warnings-off={mathtools-colon,mathtools-overbracket}]{unicode-math}
	\usefonttheme{professionalfonts}
	
	%% Fonts
	\setmainfont{Libertinus Serif}
	\setsansfont{Libertinus Sans}
	\setmathfont{Libertinus Math}
	\setmathfont[range={\mathbb,\mathfrak,\mathscr}]{XITS Math}
	\setmathfont[range={\mathcal,\mathbfcal},StylisticSet=1]{XITS Math}
	\setmathfont[range={\lbrace,\rbrace}]{STIX Two Math}
    \setmathfont[range={}]{Libertinus Math} % fix binomials

	\RequirePackage{microtype}
\else
	\ClassError{presentation}{Not supported compiler. Please use LuaLaTeX.}
\fi

%% Hacks

% hide slides from navigation

\let\beamer@writeslidentry@miniframeson=\beamer@writeslidentry
\def\beamer@writeslidentry@miniframesoff{%
    \expandafter\beamer@ifempty\expandafter{\beamer@framestartpage}{}% does not happen normally
    {%else
    % removed \addtocontents commands
    \clearpage\beamer@notesactions%
    }
}
\newcommand*{\miniframeson}{\let\beamer@writeslidentry=\beamer@writeslidentry@miniframeson}
\newcommand*{\miniframesoff}{\let\beamer@writeslidentry=\beamer@writeslidentry@miniframesoff}
\beamer@compresstrue

% align navigation to the left

\newlength\sectionsep
\setlength\sectionsep{20pt}% default 1.875ex plus 1fill

\def\sectionentry#1#2#3#4#5{% section number, section title, page
  \ifnum#5=\c@part%
  \beamer@section@set@min@width
  \box\beamer@sectionbox\hskip\sectionsep%original: 1.875ex plus 1fill%
  \beamer@xpos=0\relax%
  \beamer@ypos=1\relax%
  \setbox\beamer@sectionbox=
  \hbox{\def\insertsectionhead{#2}%
    \def\insertsectionheadnumber{#1}%
    \def\insertpartheadnumber{#5}%
    {%
      \usebeamerfont{section in head/foot}\usebeamercolor[fg]{section in head/foot}%
      \ifnum\c@section=#1%
        \hyperlink{Navigation#3}{{\usebeamertemplate{section in head/foot}}}%
      \else%
        \hyperlink{Navigation#3}{{\usebeamertemplate{section in head/foot shaded}}}%
      \fi}%
  }%
  \ht\beamer@sectionbox=1.875ex%
  \dp\beamer@sectionbox=0.75ex%
  \fi\ignorespaces}
\def\insertnavigation#1{%
  \vbox{{%
    \usebeamerfont{section in head/foot}\usebeamercolor[fg]{section in head/foot}%
    \beamer@xpos=0\relax%
    \beamer@ypos=1\relax%
    \hbox to #1{\hskip.3cm\setbox\beamer@sectionbox=\hbox{\kern1sp}%
      \ht\beamer@sectionbox=1.875ex%
      \dp\beamer@sectionbox=0.75ex%
        \hskip-\sectionsep% original -1.875ex plus 1fill
        \global\beamer@section@min@dim\z@
        \dohead%\hskip\sectionsep%
        \beamer@section@set@min@width
      \box\beamer@sectionbox\hfill\hfill %\hfill%\hskip.3cm
      }%
  }}}  

%\def\insertnavigation#1{%
%  \vbox{{%
%    \usebeamerfont{section in head/foot}\usebeamercolor[fg]{section in head/foot}%
%    \beamer@xpos=0\relax%
%    \beamer@ypos=1\relax%
%    \beamer@ypos@offset=0\relax%
%    \hbox to #1{\hskip.3cm\setbox\beamer@sectionbox=\hbox{\kern1sp}%
%      \ht\beamer@sectionbox=1.875ex%
%      \dp\beamer@sectionbox=0.75ex%
%        \hskip-1.875ex plus-1fill%
%        \global\beamer@section@min@dim\z@
%        \dohead%
%        \beamer@section@set@min@width
%      \box\beamer@sectionbox\hfil\hskip.3cm\hskip0pt plus1filll}%
%  }}} 

%% Layout

\RequirePackage{setspace}
\setstretch{1.2}
\setlength\parskip{0.5em}
\setlength\parindent{0em}
\hfuzz=100pt
\vfuzz=100pt

%\setbeamertemplate{itemize items}[triangle]
%\setbeamertemplate{itemize subitems}[triangle]
\setbeamertemplate{itemize item}{\raisebox{0.2em}{\scalebox{0.4}{\ensuremath{\blacktriangleright}}}} 
\setbeamertemplate{itemize subitem}{\raisebox{0.2em}{\scalebox{0.4}{\ensuremath{\blacktriangleright}}}} 

\usecolortheme[named=txaccent]{structure}
\setbeamerfont{footline}{series=\bfseries}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{
	\usebeamerfont{footline}%
    \usebeamercolor[fg]{structure}%
    %\rule{\textwidth}{.5pt}%
    %\vspace{5pt}%
    {\hfill\raggedleft\insertframenumber/\inserttotalframenumber}%
    \hspace{2em}%
    \vspace{3ex}%
}
\useoutertheme[subsection=false]{miniframes}

\defbeamertemplate{headline}{noheadline}{}[action]{\def\beamer@entrycode{\vspace*{-\headheight}}}

\let\oldmaketitle\maketitle
\renewcommand\maketitle{\frame[plain,noframenumbering]{\titlepage}}

\let\oldtableofcontents\tableofcontents
\renewcommand\tableofcontents{\frame[plain,noframenumbering]{\frametitle{Outline}\oldtableofcontents}}

\let\oldappendix\appendix
\renewcommand\appendix{\oldappendix\miniframesoff\frame[plain,noframenumbering]{\begin{center}\huge\structure{\appendixname}\end{center}}\miniframeson}

\newcommand{\sectionframe}{\miniframesoff\frame[plain,noframenumbering]{\sectionpage}\miniframeson}

\endinput
