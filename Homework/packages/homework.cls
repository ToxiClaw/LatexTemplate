\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{homework}

% Use KOMA article class
\RequirePackage{etoolbox, kvoptions}

\SetupKeyvalOptions {
	family = TX,
	prefix = tx,
}

\DeclareStringOption[german]{lang}[german]

\newtoggle{fast}
\DeclareOption{fast}{\toggletrue{fast}}

\newtoggle{forms}
\DeclareOption{forms}{\toggletrue{forms}}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrartcl}} % article
\DeclareOption*{\ProcessKeyvalOptions{TX}}
\ProcessOptions\relax

\LoadClass[headings=small,11pt,DIV=9]{scrartcl} % article

% Basic Includes
\RequirePackage{mathtools, amsmath, amssymb}
\RequirePackage[
	unicode,
	colorlinks=false,
	pdfborder={0 0 0},
	bookmarks,
	bookmarksopen,
	bookmarksnumbered,
	bookmarksdepth,
	pdfmenubar=false,
	pdftoolbar=false,
	pdfcreator={ToxiCore's LaTeX Template}]{hyperref}

% Compiler specific configuration
\RequirePackage{ifxetex, ifluatex}
\newif\ifxetexorluatex
\ifxetex
	\xetexorluatextrue
\else \ifluatex
	\xetexorluatextrue
\else
	\xetexorluatexfalse
\fi \fi

\ifxetexorluatex
	% Specific Includes
	\RequirePackage[babelshorthands]{polyglossia}
	\setotherlanguage{english}
	\setotherlanguage{german}
	\setdefaultlanguage{\txlang}
	\selectlanguage{\txlang}

	\RequirePackage{csquotes}
	\RequirePackage[warnings-off={mathtools-colon,mathtools-overbracket}]{unicode-math}

	%% Fonts
	\nottoggle{fast}{
		\RequirePackage{fontspec}
		\setmainfont{Libertinus Serif}
		\setmathfont{Libertinus Math}
		%\setmainfont{GFS Neohellenic}
		%\setmathfont{GFS Neohellenic Math}
		\setmathfont[range={\mathbb}]{XITS Math}
		\setmathfont[range={\mathcal}]{Latin Modern Math}
	}{}
\else
	\toggletrue{fast}
	\ClassWarning{homework}{Not supported compiler. Please use LuaLaTeX or XeTeX. This is for fast compilation only.}
%	\RequirePackage{uniinput}
\fi

\clubpenalty10000
\widowpenalty10000

% Other

\newcommand*{\tryinput}[1]{\IfFileExists{#1}{\input{#1}}{}}
\newcommand*{\tryusepackage}[1]{\IfFileExists{#1.sty}{\usepackage{#1}}{}}

% Local Includes
\RequirePackage{colors, variables, layout, theorems, exercises}

