# CONTRIBUTION GUIDE
## Purpose
This is basic LaTeX template collection.

## Filesystem Structure
Templates for different purposes are all within their own subdirectory.
Never put any sources in the main directory as this is reserved for metadata.

Within those subdirectories there commonly are multiple source files such as
- `class.cls`
- `*.sty`
- `commands.tex`
- `example.tex`
- `document.tex`
- `latexmkrc`
- `.gitignore`
- `.gitlab-ci.yml`

Here `class.cls` should have an apropritate name and `document.tex` can be named
differently fitting the purpose, e.g `solution.tex` or `booklet.tex`.
The file `example.tex` should be included into the `document.tex` and show of
the different features of the specific template and how to use them.

## Commits and Versioning
Always update the Changelog after major and fullfilled edits.
Versions are incremented by 0.1 steps if just a few features were added and by
1.0 if there are breakign changes or total rewrites.

## Licence
Do not touch the Licence!
