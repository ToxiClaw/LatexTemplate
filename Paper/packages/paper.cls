\NeedsTeXFormat{LaTeX2e}

% Use KOMA book class
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrartcl}}
\ProcessOptions\relax
\LoadClass[headings=small,12pt,DIV=9]{scrartcl}
\ProvidesClass{paper}

% Includes
\RequirePackage{mathtools, amsmath, amssymb}
\RequirePackage{scrletter, scrlayer-scrpage, setspace}
\RequirePackage{ifxetex, ifluatex, ifdraft}
\RequirePackage[nopostdot, style=super, nomain]{glossaries} % nonumberlist
\RequirePackage{totcount, etoolbox, xparse}
\RequirePackage[
	unicode,
	colorlinks=false,
	pdfborder={0 0 0},
	bookmarks,
	bookmarksopen,
	bookmarksnumbered,
	bookmarksdepth,
	pdfmenubar=false,
	pdftoolbar=false,
	pdfcreator={ToxiCore's LaTeX Template}]{hyperref}

% Local Includes
\RequirePackage{colors, variables, title, theorems}

% Class Macros
\newif\ifxetexorluatex
\ifxetex
	\xetexorluatextrue
\else \ifluatex
	\xetexorluatextrue
\else
	\xetexorluatexfalse
\fi \fi

% Configuration
\ifxetexorluatex
	% Specific Includes
	\RequirePackage[babelshorthands]{polyglossia}
	\setdefaultlanguage{german}
	\PolyglossiaSetup{german}{indentfirst=true}
	
	\RequirePackage[
		backend=biber,
		style=alphabetic,
		abbreviate=false,
		giveninits,
		url=true]{biblatex}
	
	\RequirePackage{csquotes}
	\MakeOuterQuote{"}

	\RequirePackage{fontspec}
	\RequirePackage[capitalise, nameinlink]{cleveref}
	\RequirePackage[warnings-off={mathtools-colon,mathtools-overbracket}]{unicode-math}

	%% Fonts
	\setmainfont{GFS Neohellenic}
	\setmathfont{GFS Neohellenic Math}
	\setmathfont[range={\mathbb,\mathcal}]{XITS Math}
\else
	\ClassError{paper}{Not supported compiler.}{Please use LuaLaTeX or XeTeX.}
\fi

% Header and Footer
\clearscrheadfoot
\ihead[]{\normalfont \thetitle}
\ohead{\normalfont \headmark}
\cfoot[\pagemark]{\pagemark}
\automark{section}
\pagestyle{scrheadings}

%% Layout
\addtokomafont{disposition}{\bfseries\rmfamily\boldmath}
%\addtokomafont{disposition}{\centering \normalfont\scshape}
\newcommand{\mark@section}{\textcolor{red!40!black}{$\blacktriangleright$}}
\addtokomafont{section}{\large\normalfont\mark@section~}

% Paragraphs
\setstretch{1.05}
\setlength\parskip{0pt}
\setlength\parindent{1.5em}

\endinput
