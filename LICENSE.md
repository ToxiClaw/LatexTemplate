# LICENCE and TERMS OF SERVICE for LaTeX Templates

## Structure / Definitions
Textfile
:   A text file with a specific name 'X' may be called 'X', 'X.md', 'X.txt' or 'X.rtf' and may be reffered to with no respect to capital letters.

Project's Root Directory
:   This 'LICENCE' textfile file is located at the project's root directory.

Sublicences / Sublicence File
:   Textfiles called 'LICENCE' within subdirectories of the project's root directory will be called sublicences and/or sublicence file.

## Sublicensing
For the specific Licences and Terms of Service as well as information about Copyright holders there exist detailed raw textfiles called "LICENCE" within the specific subdirectories. Those are the mentioned sublicences.

Each of those terms will have to be followed for the contents of the specific subdirectory and their subdirectories in respect. However they do not take effect upon their neighboring (in respect to the project root directory) subdirectories.

## Copyright
Copyright 2018 - Adrian Gallus.
There may be more copyright holders who are mentioned within the specific sublicence file.

## Sharing
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

## No Waranty
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
THIS IS TRUE EVEN IF THE SPECIFIC SUBDIRECTORY DOES NOT MENTION SO AND/OR DOES NOT CONTAIN A LICENCE FILE.
