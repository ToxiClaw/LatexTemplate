# Version History of `Paper`

## Version 2.0
 + imrpoved project structure

## Version 1.0
 + basic template
 + paper example
 + custom title
 + frontmatter
 + bibliography
 + definitions and theorems
 + glossaries
 + commands
